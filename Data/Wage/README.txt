WAGE: An extract from the March, 1995 Current Population Survey of the U. S. Census Bureau. 
1289 observations, 8 variables, 

including w (wage), 

fe female indicator variable), 

nw (nonwhite indicator),

un ( (union indicator), 

ed (years of schooling), 

ex (years of potential experience), 

age (age), 

wk (weekly earnings indicator variable)
 
