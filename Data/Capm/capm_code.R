# Capm estimation

# Estimation of: risk_premium of industry = beta * market_premium

capm_food_model <- lm(rfood ~ -1 + rmrf, data=Capm)

summary(capm_food_model)

capm_dur_model <- lm(rdur ~ -1 + rmrf, data=Capm)

summary(capm_dur_model)

capm_con_model <- lm(rcon ~ -1 + rmrf, data=Capm)

summary(capm_con_model)

# an excess return on the market of, say, 10% corresponds to an expected excess return on the food, durables and construction portfolios of 7.9, 11.1 and 11.6% respectively.

#attach(Capm)
#cov(rfood + rf, rmrf + rf) / var(rmrf + rf)
#cov(rdur + rf, rmrf + rf) / var(rmrf + rf)
#cov(rcon + rf, rmrf + rf) / var(rmrf + rf)
