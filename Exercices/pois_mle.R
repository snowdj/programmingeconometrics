data_mle <- read.table("~/Dropbox/Documents/Work/TDs/Applied Econometrics with R/Exercices/data_mle.csv", header=TRUE, quote="\"")

data_mle <- data_mle$x


poissonLogLik <- function(lambda, y){
 result <- 0
 n <- length(y)
 for(i in 1:n){
  result <- result + y[i]
 }
 result <- result * log(lambda) - (n * lambda)
 return(-result)
} 

param <- seq(0.1, 10, length.out = length(data_mle))

plot(param, poissonLogLik(param, data_mle), type = "l", col = "blue", lwd = 2)

optim(par = 1, fn = poissonLogLik, y = data_mle, method="Brent", lower=0, upper=max(data_mle))


poissonLogLik2 <- function(lambda, y){
  n <- length(y)
  result <- sum(y)*log(lambda) - n*lambda
  return(-result)
}

optim(par = 1, fn = poissonLogLik2, y = data_mle, method="Brent", lower=0, upper=max(data_mle))

poissonLogLik3 <- function(lambda, y){
  result <- sum(log(dpois(y, lambda)))
  return(-result)
}

optim(par = 1, fn = poissonLogLik3, y = data_mle, method="Brent", lower=0, upper=max(data_mle))
