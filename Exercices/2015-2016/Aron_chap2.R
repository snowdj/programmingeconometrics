# Exercice 1

a <- c(6, 3, 8, 9) # vector a
b <- c(9, 1, 3, 5) # vector b

c <- a + b # we add the vector a to the vector b
print(c)

d <- a - b # we subtract the vector b to the vector a
print(d)


# Exercice 2

a * b
# I tried a * b in the command prompt, I get [1] 54  3 24 45 and it's not the dot product

a %*% b 
# dot product of a and b


# Exercice 3

X <- matrix(2, nrow = 30, ncol = 30 )
Y <- matrix(4, nrow = 30, ncol = 30 )

X * Y 
# again it's not the dot procuct but just a new matrice composed of 8's (2*4)

X %*% Y
# dot product of the two matrices X and Y

# these two operators don't do the same thing. %*% is the dot product while * is just a simple product


# Exercice 4

a <- "Laurent"
b <- "Aron"
paste(a,b)
# this command put the two variables side to side with the characters associated


# Exercice 5

a <- 8
b <- 3
c <- 19

a > b
# TRUE : a is superior to b

a == b
# FALSE : a is not equal to b

a != b
# TRUE : a is different to b

a < b
# FALSE : a is not inferior to b

(a > b) && (a < c)
# TRUE : && means AND, and it's corret because a is superior to b AND a is inferior to c

(a > b) && (a > c)
# FALSE : a is superior to b but a is not superior to c

(a > b) || (a < b)
# TRUE : || means OR, and a is superior to b so it's correct, even if a is not inferior to b (and it's logical)


# Exercice 6

A <- matrix(c(9, 4, 12, 5, 0, 7, 2, 6, 8, 9, 2, 9), nrow = 4, byrow = TRUE)

print(A)

A >= 5
# this command shows where in the matrix the values are superior or equal to 5

A[ , 2]
# this command shows the column 2

t(A)
# this command gives us the transpose of the matrix A


# Exercice 7

# Let's create a new vector called v :
v <- rbind(2, 9, 0, 11)
print(v)

# Now we add v to A so we have a new matrix 
A <- cbind(A,v)
print(A)

# we have the system A * X = B
# so by doing the product with the inverse of the matrix A we get :
# A-1 * A * X = A-1 * B
# I * X = A-1 * B
# X = A-1 * B

solve(A)
# this command gives us the inverse of A

B <- rbind(7, 18, 1, 0)
print(B)

X <- solve(A) %*% B
print(X)

# we have the solutions of the system



