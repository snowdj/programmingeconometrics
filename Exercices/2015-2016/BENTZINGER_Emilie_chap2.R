#EXercice 1 :

#vertor a
a <- cbind(6,3,8,9)
a

#vector b
b <- cbind(9,1,3,5)
b

#addition of a and b
c <- a + b
c

#differnce of a and b
d <- a - b
d




#Exercice 2 :

a * b
# it's not a dot product, it's a vector
# with the first coefficient which is a multiplication of the first coefficient of a and of the first coefficient of b
# and the second coefficient a multiplication of the second coefficient of a and second coefficient of b
# same for the third and fourth

e <- rep(b)

a %*% e 
# dot product of a and b


# other method
a %*% t(b)




#Exercice 3 :

A <- matrix(2, nrow=30, ncol=30) 
A
B <- matrix(4, nrow=30, ncol=30)
B

A * B
# it's not a dot product but a Hadamard product

A %*% B
# dot product

# they are not the same, they don't do the same product.




#Exercice 4 :
a <- "Emilie"
b <- "BENTZINGER"
paste(a,b)
# This comment bonds strings together
# here my first name and my surname, so R returns my full name




#Exercice 5 :
a <- 8
b <- 3
c <- 19

a > b
#this line checks if 'a' is higher than 'b'
#if it's the case, then R returns 'true', otherwise 'false'.

a == b
#this line checks if 'a' is exactly equal to 'b'
#if it's the case, then R returns'true', otherwise 'false'.

a != b
#this line checks if 'a' is not equal to 'b'
#if it's the case, then R returns 'true', otherwise 'false'.

a < b
#this line checks if 'a' is less than 'b'
#if it's the case, then R returns 'true', otherwise 'false'.

(a > b) && (a < c)
#this line checks if 'a' is higher than 'b' AND (&&) if 'a' is less than 'c'
#if at least one of the specified conditions is false, then R will return 'false', so if R returns 'true' ALL the specified conditions are true.

(a > b) && (a > c)
#this line checks if 'a' is higher than 'b' AND (&&) if 'a' is higher than 'c'
#if at least one of the specified conditions is false, then R will return 'false', so if R returns 'true' ALL the specified conditions are true.

(a > b) || (a < b)
#this line checks if 'a' is higher than 'b' OR (||) if 'a' is less than 'b'
#if at least one of the specified conditions is true, then R will return 'true', so if R returns 'false' ALL the specified conditions are false.




#Exercice 6 :

A <- matrix(c(9, 4, 12, 5, 0, 7, 2, 6, 8, 9, 2, 9), nrow = 4, byrow = TRUE)
A

#1.
A >= 5
# it's the same matrix, except that in place of the numbers, there are 'true' or 'false'
# if the condition is met, then R returns 'true', otherwise 'false' fot each coefficient
# here if the number in the A matrix is >= 5, R returns 'true'
# and when the number is < 5, R returns 'false'


#2.
A[ , 2]
# R returns the second column
# with this command, R returns a specific line, column or coefficient


#3.
t(A)
# it's the transpose of A




#Exercice 7 : 

A <- matrix(c(9, 4, 12, 2, 5, 0, 7, 9, 2, 6, 8, 0, 9, 2, 9, 11), nrow = 4, byrow = TRUE)
A
B <- t(cbind(7, 18, 1, 0))
B

#inverse of A matrix :
solve(A)

# AX = B
# X = A^(-1) * B :
(solve(A)) %*% B

# otherwise we can solve the system with this command :
solve(A, B)
