> R.Version()
$platform
[1] "x86_64-apple-darwin13.4.0"

$arch
[1] "x86_64"

$os
[1] "darwin13.4.0"

$system
[1] "x86_64, darwin13.4.0"

$status
[1] ""

$major
[1] "3"

$minor
[1] "2.2"

$year
[1] "2015"

$month
[1] "08"

$day
[1] "14"

$`svn rev`
[1] "69053"

$language
[1] "R"

$version.string
[1] "R version 3.2.2 (2015-08-14)"

$nickname
[1] "Fire Safety"