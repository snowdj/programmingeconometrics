#excice 1
a<-c(6,3,8,9)
b<-c(9,1,3,5)
a
b
a+b
a-b
#exercice 2
a
b
a*b
#exercice 3
A<-matrix(2,nrow=30,ncol=30)
A
#exercice 4
rm(list=ls())
a<-"li"
b<-"tian"
class(a)
class(b)
paste(a,b)
#exercice 5
rm(list=ls())
a<-8
b<-3
c<-19
a>b
a==b
a!=b
a<b
(a>b)&&(a<c)
(a>b)&&(a>c)
(a>b)||(a<b)
#exercice 6
A<-matrix(c(9,4,12,5,0,7,2,6,8,9,2,9),nrow=4,byrow=TRUE)
A
A>=5
A[ ,2]
t(A)
#exercice 7
r<-rbind(2,9,0,11)
r
cbind(A,r)
A<-cbind(A,r)
A
solve(A)
B<-rbind(7,18,1,0)
B
C<-solve(A)
C
C%*%B
