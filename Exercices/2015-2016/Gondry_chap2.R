# Exercise 1

a <- cbind (6, 3, 8, 9)
b <- cbind (9, 1, 3, 5)
c <- rbind (a,b)
d <- (a-b)


# Exercise 2

a*b

# That command multiplied each coefficient (i,j) from
# the vector a by the same coefficient (i,j) of the vector b.
# For example, 6*9 = 54 (A(1,1) * B(1,1))
# To get the dot product, we have to use %*% :

t(a) %*% b
a %*% t(b)


# Exercise 3

E <- matrix (2, nrow = 30, ncol = 30)
F <- matrix (4, nrow = 30, ncol = 30)

E*F
# As in the previous exercise, using the * operator multiplies
# each coefficient from the E matrix by the coefficient situated
# at the same place from the F one.

# To get the dot product, we should once again use %*%
E%*%F

# The %*% operator is used to multiply matrices, while * consider matrices
# as lists, and multiplies each component by the component situated at the 
# same place of the second matrix.


# Exercise 4

a <- "Gondry"
b <- "Manon"
paste (a,b)
# That command place the object a next to b.


# Exercise 5

a <- 8
b <- 3
c <- 19

a > b
# Checks whether the element a is greater than b or not.

a == b
# Checks if a is equivalent to b.

a < b
# Checks if a is less than b.

(a > b) && (a < c)
# Checks both statements (both of them are true so the console shows TRUE).

(a > b) && (a > c)
# Checks both statements (one of them is true but the other one isn't,
# the console shows FALSE).

(a > b) || (a < b)
# Checks if at least one of the two statements are true (here, one of them
# is, so the console shows TRUE, even if the second one is wrong).


# Exercise 6

A <- matrix (c(9, 4, 12, 5, 0, 7, 2, 6, 8, 9, 2, 9), nrow = 4, byrow = TRUE)

A >= 5
# That command checks whether each coefficient of the matrix is greater
# or equal to 5.

A [,2]
# That command displays the second column of the matrix A.

#t(A) is the command that shows the transpose of the matrix.


# Exercise 7

A <- matrix (c(9, 4, 12, 2, 5, 0, 7, 9, 2, 6, 8, 0, 9, 2, 9, 11), nrow = 4, byrow = TRUE)
B <- matrix (c(7, 18, 1, 0), nrow = 4, byrow = TRUE)

x <- as.integer
y <- as.integer
z <- as.integer
t <- as.integer
X <- matrix (c(x,y, z, t), nrow = 4, byrow = TRUE)

# Pour inverser la matrice A, utilisons solve(A)
InvA <- solve(A)
X <- InvA %*% B