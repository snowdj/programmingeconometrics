###### Exercice 1 ######
  
  # Create vectors a & b #
  a <- c(6,3,8,9)
  b <- c(9,1,3,5)
  
  # Addition of both vectors #
    (c <- a+b)
  
  # Substraction of both vectors #
    (d <- a-b)
  
  
  
  ###### Exercice 2 ######
    
  # Still using a & b from the first exercise #
    
  # Operation with the sign * #
    (a*b)
  #It gives a normal multiplication and not the dot product #
  
  # To get the dot product, let's use the right command #
    (a%*%b)
  
  
  
  ###### Exercice 3 ######
  
  # Creation of the matrix (30,30) filed with 2's #
  (matrix1 <- matrix(2:2, ncol=30, nrow=30))
  
  #Creation of the matrix (30,30) filed with 4's #
  (matrix2 <- matrix(4:4, ncol=30, nrow=30))
  
  #Operation with sign * #
  (matrix1 * matrix2)
  # R make a simple multiplication between each terms of the matrix 1 and 2 #
  
  #Calculate the dot products #
  (matrix1t %*% matrix2)
  
  #R make the dot product between transpose matrix 1 and matrix 2 #
  
  #We can conclude that the operator * tells R to make a simple multiplication and the operator %*% tells R to make the dot products between 2 vectors or 2 matrix#
  
  
  
  ###### Exercise 4 ######
  
  # Creation variable a #
  (a <- c("Nathan"))
  
  # Creation variable b #
  (b <-("Caminade"))
  
  # Test of the command #
  (paste(a,b))
  
  # The command link the variables between them and now I can read my entire name #
  
  
  
  ###### Exercice 5 ######
  
  # Create the variables #
  (a <- 8)
  (b <- 3)
  (c <- 19)
  
  # Type all the commands asked #
  (a>b)     # It checks if a superior than b #
  (a==b)    # It checks if a = b #
  (a != b)  # It checks if a is different from b #
  (a<b)     # It checks if a inferior than b #
  ((a > b) && (a < c)) # It checks if a > b AND a < c are both true, if one is false, it will return false #
  ((a > b) && (a > c)) # It checks if a > b AND  a > c are both true, if one is false, it will return false # 
  ((a > b) || (a < b)) # It checks if a > b OR a < b is true or false #
  
  
  # Conclusion : all this command returns "TRUE" or "FALSE", it's only logical comparisons from the logical class#
  
  
  
  
  ###### Exerce 6 ######
  
  # Define the matrix A #
  (A <- matrix(c(9,4,12,5,0,7,2,6,8,9,2,9), ncol=3, nrow=4, TRUE))
  
  
  # Question 1 #
  (A >= 5) # It compares each numbers in the matrix to see if it's upper or equal 5 # 
  
  # Question 2 #
  (A[ , 2]) # It gives the column number 2 #
  
  # Question 3 #
  (At <- aperm(A)) # The command "aperm" gives the transpose of the matrix #
  
  
  
  ###### Exercice 7 ######
  
  # Define all the variables #
  (A <- matrix(c(9,4,12,2,5,0,7,9,2,6,8,0,9,2,9,11), ncol=4, nrow=4, TRUE))
  (X <- matrix(c("x","y","z","t"), ncol=1, nrow=4, TRUE))
  (B <- matrix(c(7,18,1,0), ncol=1, nrow=4, TRUE))
  
  #Determine the invert of A #
  (Ai <- solve(A))
  
  # Now if we multiply each side by Ai, we will have the solution of our problems because AxAi gives the matrix Identity, so let's determine the product between B and Ai #
  (S <- Ai %*% B)
  
  # We can now conlude that x = -4,2028751 ; y = -6,2285714 etc #
  (X = S)
  
  
  ###############################
  
  
  
  