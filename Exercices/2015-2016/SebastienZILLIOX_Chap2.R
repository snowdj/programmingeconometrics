R.Version()
## Exercice 1
a<-cbind(6,3,8,9)
a
b<-cbind(9,1,3,5)
b
c<-a+b
c
d<-a-b
d
## Exercice 2
a*b
## On a la multipli� chaqu'un des nombresdu vecteur 
## avec le nombre se trouvant � la m�me place dans 
## l'autre vecteur
t(a)%*%b 
## Exercice 3
A<-matrix(2,nrow=30,ncol=30)
B<-matrix(4,nrow=30,ncol=30)
A
B
t(A)%%B
t(B)%%A
A*B
B*A
## Exercice 4
a<-c("Sebastien")
a
b<-c("ZILLIOX")
b
paste(a,b)
## Cette commande permet de mettre les listes dans un
## certain ordre sans changer l'ordre des objets
## qui sont contenus dans ces listes
## Exercice 5
a<-8
a
b<-3
b
c<-19
c
a>b ## est-ce que a est sup�rieur � b ?
a==b ## est-ce que a est �gal � b ?
a!=b ## est-ce que a est diff�rent de b ?
help("!=")
a<b ## est-ce que a est inf�rieur � b ?
(a > b) && (a < c) ## est-ce que a inf�rieur � b et 
                   ## a inf�rieur � c ? 
(a > b) && (a > c)## est-ce que a inf�rieur � b et
                  ## ## a sup�rieur � c ? 
(a > b) && (a < b)## est-ce que a inf�rieur � b et
                  ## a inf�rieur � b ? 
## On a pour retour uniquement VRAI ou FAUX � ces
## test
## Exercice 6
A<-matrix(c(9,4,12,5,0,7,2,6,8,9,2,9), nrow = 4,ncol = 3,byrow = TRUE)
A
A>=5
## Cette commande indique pour chaque nombre de la 
## matrice si celui-ci est sup�rieur � 5
A[,2]
## Cette commande nous donne la deuxi�me colonne de 
## la matrice A sous forme de ligne et uniquement 
## celle-ci
t(A) ## On utilise la commande " t()" pour
     ## transposer une matrice
## Exercice 7

A<-matrix(c(9,4,12,2,5,0,7,2,9,6,8,9,0,2,9,11), nrow = 4,ncol = 4,byrow = TRUE)
A<-solve(A)
A
X<-A%*%D
X
