biNormdens <- function(theta, x, y, n){
  mux <- theta[1]
  muy <- theta[2]
  sx <- theta[3]
  sy <- theta[4]
  rho <- theta[5]
  return((1 / (2 * pi * sx * sy * sqrt(1 - rho^2))) * exp(-1/(2 * (1 - rho^2)) *((x - mux)/sx)^2+((y-muy)/ sy)^2-2*rho*((x - mux)/ sx) * (y - muy)/ sy))
}

n <- length(c(x,y))

theta <- c(0, 0, 1, 1, 0.5)

x <- seq(1:50)
y <- seq(1:50)


persp(x, y, biNormdens(theta, x, y, n))

logL <- function(theta, x, y, z, N){
  contrib <- 0
  beta <- head(theta, -1) # Every element but the last one
  sigma <- tail(theta, 1) # Only the last element
  for (i in 1:N){
    contrib <- contrib + (y[i] - beta[1] * x[i, 1] - beta[2] * x[i, 2])^2
  }
  L <- -(1/(2*sigma^2)*contrib) - 1/2 * N * log(2*pi) - N * log(sigma)
  return(-L)
}
